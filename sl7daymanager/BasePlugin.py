import logging
from ruamel import yaml


class BasePlugin:
    """
    Provides common plugin functionality. Plugins should inherit from BasePlugin.
    """

    def __init__(self, cfg):
        self.default_config = self.load_config()
        self.enabled = True
        self.log = logging.getLogger(__name__)
        self.cfg = cfg

        if hasattr(self, "requires"):
            missing_plugins = []
            for required_plugin in self.requires:
                if required_plugin not in self.cfg["Global"]["enabled_plugins"]:
                    missing_plugins.append(required_plugin)
            if missing_plugins:
                self.enabled = False
                self.log.error("Plugin '{}' requires these plugins: {}, "
                               "but the following required plugins are missing: {}"
                               "".format(self.__class__.__name__, self.requires, missing_plugins))
                self.log.info("Plugin '{}' will be disabled due to missing dependencies."
                              "".format(self.__class__.__name__))

    @classmethod
    def load_config(cls):
        """
        Load the default config for this plugin.
        :return: Dictionary of default config values.
        """
        try:
            return yaml.load(open("plugins/{}/default_config.yaml".format(cls.__name__)).read(),
                             Loader=yaml.RoundTripLoader)
        except FileNotFoundError:
            return {}

    def get_config(self, key, default=None):
        """
        Get a config key for this plugin. First try to lookup the key in self.cfg. If it's not there, look for it in
        self.default_config. If it's not there, return the 'default' method argument.
        :param key: The config key to get from this plugin's config.
        :param default: Last-resort default value.
        :return: The relevant config value.
        """
        class_config = self.cfg.get(self.__class__.__name__, self.default_config)
        return class_config.get(key, self.default_config.get(key, default))

    def get_global_config(self, key, default=None):
        """
        Get a value from the global config.
        :param key: The config key to get from the global config.
        :param default: Last-resort default value.
        :return: The relevant config value.
        """
        return self.cfg["Global"].get(key, default)
