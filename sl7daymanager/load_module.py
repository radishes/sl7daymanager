import importlib


def get_class_by_name(dotted_module_name, class_name):
    """
    Use the name of a class object to get the class.
    :param dotted_module_name: 'plugins.Telnet.Telnet'
    :param class_name: 'Telnet'
    :return: A class.
    """
    try:
        return getattr(importlib.import_module(dotted_module_name), class_name)
    except ImportError:
        return None
