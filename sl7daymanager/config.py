import os
import collections
import logging
from ruamel import yaml


log = logging.getLogger(__name__)


def load_config(path):
    """
    Load yaml configs and merge them.
    :param path: A path to a single yaml file to load, or a directory containing yaml files, which will all be 
    loaded.
    :return: A merged config of all loaded yaml files.
    """
    config_data = {}
    files_list = []
    parsed_files = 0
    if os.path.isfile(path):
        # Load a single file.
        files_list.append(path)
    elif os.path.isdir(path):
        # Load a directory of config files. YAML files will be loaded. Files that fail YAML parsing will be skipped.
        log.debug("Loading all config files in path '{}'.".format(path))
        for filename in os.listdir(path):
            files_list.append(os.path.join(path, filename))
        files_list.sort()  # Load config files in alphabetic order of filename.
    for file_path in files_list:
        log.debug("Loading config file '{}'.".format(file_path))
        with open(file_path) as f:
            try:
                config = yaml.load(f.read(), Loader=yaml.Loader)
            except yaml.parser.ParserError as e:
                log.warning("Skipping unparseable config file '{}'. Is this a valid yaml file? "
                            "The error was:\n{}".format(file_path, e))
                continue
            dict_merge(config_data, config)
            parsed_files += 1
    log.debug("Loaded config values from {} {}.".format(parsed_files, "file" if parsed_files == 1 else "files"))
    return config_data


def dict_merge(dct, merge_dct):
    """
    Recursive dict merge. Inspired by :meth:``dict.update()``, instead of
    updating only top-level keys, dict_merge recurses down into dicts nested
    to an arbitrary depth, updating keys. The ``merge_dct`` is merged into
    ``dct``.
    :param dct: dict onto which the merge is executed
    :param merge_dct: dct merged into dct
    """
    for k, v in merge_dct.items():
        if (k in dct and isinstance(dct[k], dict)
                and isinstance(merge_dct[k], collections.Mapping)):
            dict_merge(dct[k], merge_dct[k])
        else:
            dct[k] = merge_dct[k]
