from pubsub import pub
from sqlalchemy import Column, DateTime, Integer, Text
import db
from BasePlugin import BasePlugin


class CommandLogger(BasePlugin):
    """
    Log console and telnet commands to the datastore.
    """
    def __init__(self, cfg):
        super().__init__(cfg)
        pub.subscribe(self.console_command, "console_command")

    def console_command(self, msg_data):
        """
        Process console and telnet messages.
        """
        console_cmd = ConsoleCommand(timestamp=msg_data["datetime"],
                                     user=msg_data["user"],
                                     src_address=msg_data["address"],
                                     port=int(msg_data["port"]),
                                     command=msg_data["cmd"],
                                     arguments=msg_data["args"])
        db.session.add(console_cmd)
        db.commit_session()


class ConsoleCommand(db.Base):
    """
    A command issued by an admin via telnet or console.
    """
    __tablename__ = 'console_command'
    id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime)
    user = Column(Text)
    src_address = Column(Text)
    port = Column(Integer)
    command = Column(Text)
    arguments = Column(Text)

    def __repr__(self):
        return "ConsoleCommand<timestamp='{timestamp}', command='{command}', arguments='{arguments}'".\
            format(**self.__dict__)
