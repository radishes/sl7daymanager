from ipcalc import Network
from pubsub import pub
from sqlalchemy import Column, Integer, String
import db
from BasePlugin import BasePlugin


class NetworkBlacklist(BasePlugin):
    """
    Allows IP addresses or IP ranges to be blacklisted from the server. Players who join from a blacklisted network
    will be automatically banned.
    """
    def __init__(self, cfg):
        super().__init__(cfg)
        self.validated_ips = []
        self.network_blacklist = set()
        pub.subscribe(self.player_state, "player_state")
        pub.subscribe(self.player_disconnect, "player_disconnect")
        pub.subscribe(self.blacklist_network, "blacklist_network")

    def post_init(self):
        for blacklisted_network in self.get_config("blacklisted_networks", []):
            self.network_blacklist.add(str(blacklisted_network))
        self.log.info("Loaded {} blacklisted networks from configs.".format(len(self.network_blacklist)))

    def player_state(self, msg_data):
        """
        Check if this player has a blacklisted IP, and ban them if they do. If not, add them to the list of validated
        IPs so that they don't need to be checked again for this session.
        """
        ip = msg_data["ip"]
        if ip in self.validated_ips:
            return
        player_name = msg_data["name"]
        steamid = msg_data["steamid"]
        self.network_blacklist.update(db.session.query(BannedNetwork.network).all())
        banned = False
        for banned_network in self.network_blacklist:
            try:
                if ip in Network(banned_network):
                    self.log.info("Banning player '{}' from banned IP list.".format(player_name))
                    pub.sendMessage("ban_player", steamid=steamid, msg=self.get_config("ban_message"))
                    pub.sendMessage("say_server", msg=self.get_config("ban_announcement").format(player_name))
                    banned = True
                    break
            except ValueError:
                pass  # Invalid IP

        if not banned:
            self.validated_ips.append(ip)

    def player_disconnect(self, msg_data):
        """
        When a player disconnects from the server, remove them from the list of validated players.
        """
        self.log.debug("Processing player_disconnect message: {}".format(msg_data))
        steamid = msg_data["steamid"]
        try:
            self.validated_ips.remove(steamid)
        except ValueError:
            pass  # Value not in list.

    def blacklist_network(self, network):
        """
        Add a network or IP to
        :param network: A string representing a single IP address ("1.2.3.4") or a network in slash notation
        ("1.2.3.4/24")
        :return: None
        """
        chat_msg = BannedNetwork(network=network)
        db.session.add(chat_msg)
        db.commit_session()
        self.validated_ips.clear()


class BannedNetwork(db.Base):
    """
    An IP or IP range that is blacklisted from the server.
    """
    __tablename__ = 'banned_network'
    id = Column(Integer, primary_key=True)
    network = Column(String(24))

    def __repr__(self):
        return "<BannedNetwork(network='{}')>".format(self.network)
