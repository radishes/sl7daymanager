"""
Helper classes to parse log lines.
"""
import re
import datetime


class ParsedMsg:
    def __init__(self):
        self.msg_type = ""
        self.msg_data = {}


class Parsers:
    pre_parsers = []
    parsers = []

    @classmethod
    def init_parsers(cls, parser_definitions):
        for parser_name, data in parser_definitions['pre_parsers'].items():
            cls.pre_parsers.append(ConsoleParser(parser_name, data['regex']))
        for parser_name, data in parser_definitions['parsers'].items():
            cls.parsers.append(ConsoleParser(parser_name, data['regex']))

    @classmethod
    def parse_msg(cls, msg):
        """
        Matches at most one pre-parser and one parser per message.
        :param msg:
        :return:
        """
        parsed_msg = ParsedMsg()
        pre_parsed = {}
        parsed = {}
        if not msg:
            return {}
        for pre_parser in cls.pre_parsers:
            pre_parsed = pre_parser.match_re(msg)
            if pre_parsed:
                parsed_msg.msg_data.update(pre_parsed)
                break
        for parser in cls.parsers:
            if not parsed_msg.msg_data:
                parsed = parser.match_re(msg)
            elif 'remaining' in pre_parsed:
                parsed = parser.match_re(pre_parsed['remaining'])
            if parsed:
                parsed_msg.msg_type = parser.regex_name
                parsed_msg.msg_data.update(parsed)
                break
        if "datetime" in parsed_msg.msg_data:
            parsed_msg.msg_data["datetime"] = datetime.datetime.strptime(parsed_msg.msg_data["datetime"],
                                                                         "%Y-%m-%dT%H:%M:%S")
        return parsed_msg


class ConsoleParser:
    def __init__(self, regex_name, regex):
        self.regex_name = regex_name
        self.regex = re.compile(regex)

    def __str__(self):
        return self.regex_name

    def match_re(self, msg):
        match = self.regex.match(msg)
        if not match:
            return {}
        else:
            return match.groupdict()
