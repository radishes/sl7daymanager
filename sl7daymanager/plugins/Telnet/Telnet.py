import re
import sys
import telnetlib
import time
from socket import gaierror
from pubsub import pub
import config
from BasePlugin import BasePlugin
from plugins.Telnet.Parsers import Parsers


class Telnet(BasePlugin):
    """
    Listens to the telnet feed of the 7dtd server, parses interesting messages, and sends them out to subscribing
    plugins. Provides the ability for other plugins to issue commands to the server.
    """
    def __init__(self, cfg):
        super().__init__(cfg)
        self.tn = None  # Telnet connection to the 7dtd server.

        pub.subscribe(self.send_cmd, "telnet_cmd")
        pub.subscribe(self.say, "say_server")

    @staticmethod
    def make_cmd(msg):
        """
        Format a string so that it can be issued to the 7dtd server via telnet.
        :param msg: String of the command to format.
        :return: Formatted command msg.
        """
        return bytes(msg.encode("utf-8")) + b"\r\n"

    def send_cmd(self, cmd):
        """
        Send a command to the 7dtd server. If safe mode is enabled, only safe commands will be actually issued to the
        server.
        :param cmd: String of the command to send to the server.
        """
        cmd_is_safe = True
        if self.get_global_config("safe_mode"):
            # Safe mode is enabled; validate whether the command is safe.
            cmd_is_safe = False
            for safe_cmd in self.get_config("safe_cmds"):
                if cmd.startswith(safe_cmd):
                    cmd_is_safe = True
        if cmd_is_safe:
            try:
                self.tn.write(self.make_cmd(cmd))
            except (BrokenPipeError, ConnectionResetError, AttributeError) as e:
                # Telnet connection is unavailable. Eat the error and do not retry.
                self.log.debug("Failed to send command: '{}' due to error: {}".format(cmd, e))
        else:
            self.log.info("Safe mode is enabled; not sending message to server: {}".format(cmd))

    def say(self, msg, color="default"):
        """
        Send a chat message to the server as 'Server'.
        :param msg: Chat message to send.
        :param color: RGB hex code of color for chat message, or None for no color (white). Example: "FF2200". If a
        say message was already sent with color information (i.e., 'say [ABC123]hello[FFFFFF]'), then the color
        parameter is ignored.
        """
        color_re = re.compile(r"\[[a-fA-F0-9]{6}\]")
        if color:
            color = color.upper()

        if color_re.match(msg):
            # Message already has a color string added.
            pre_color = ""
            post_color = ""
        elif color == "DEFAULT":
            # Use default configured color string.
            pre_color = "[{}]".format(self.get_config("server_chat_color"))
            post_color = "[FFFFFF]"
        elif color:
            # Use the color string provided in the parameter.
            pre_color = "[{}]".format(color)
            post_color = "[FFFFFF]"
        else:
            # Don't use a color string if color parameter is False.
            pre_color = ""
            post_color = ""
        self.send_cmd('say "{}{}{}"'.format(pre_color, msg, post_color))

    def start(self):
        """
        Entry point for Telnet module. Kicks off run() and handles exceptions that bubble up. Attempts to endlessly
        restart the telnet connection when it dies.
        """
        retry_sleep = 5
        self.log.debug("Loading console parsers...")
        Parsers.init_parsers(config.load_config("plugins/Telnet/parsers.yaml"))

        while True:
            try:
                self.run()
            except ConnectionResetError as e:
                self.log.info("Connection reset occurred: {}".format(e))
            except ConnectionRefusedError as e:
                self.log.info("Connection refused: {}".format(e))
                self.log.info("Sleeping for {} seconds before reconnecting...".format(retry_sleep))
            time.sleep(retry_sleep)

    def run(self):
        """
        Telnet module main loop. Connects to the telnet server and processes the messages that come across the wire.
        """
        host = self.get_config("7dtd_server_telnet_address")
        port = self.get_config("7dtd_server_telnet_port")
        while True:
            self.log.info("Connecting to telnet server {}:{}.".format(host, port))
            try:
                self.tn = telnetlib.Telnet(host, port)
            except gaierror:
                self.log.error("Unable to connect to the telnet server. '{}:{}' appears to be an illegal address:port "
                               "combination. Review the '7dtd_server_telnet_address' and '7dtd_server_telnet_port' "
                               "settings in the Telnet section of your config file. Exiting.".format(host, port))
                sys.exit(1)
            self.log.debug("Connected to server. Waiting for password prompt...")
            self.tn.read_until(b"Please enter password:").decode("utf-8")
            self.log.debug("Sending password to server...")
            self.tn.write(self.make_cmd(self.get_config("7dtd_server_telnet_password")))

            while not self.tn.eof:
                password_response = self.tn.read_until(b"\r\n").decode("utf-8").strip()
                if password_response != "":
                    break
            if password_response == "Password incorrect, please enter password:":
                self.log.error("Unable to log into the telnet server with the provided telnet password. Check the "
                               "'7dtd_server_telnet_password' in the Telnet section of your config file. Exiting.")
                sys.exit(1)
            elif password_response == "Logon successful.":
                self.log.info("Telnet login successful.")
                pub.sendMessage("telnet_connected")
            else:
                self.log.error("Unrecognized response from telnet server:\n{}".format(password_response))
            while not self.tn.eof:
                raw_msg = self.tn.read_until(b"\r\n").decode("utf-8").strip()
                parsed_msg = Parsers.parse_msg(raw_msg)
                # Have parsed_msg have an "accumulate" flag if the message is composed of more than one line.
                # Multi-line parsers will have sub-parsers to detect all the various types from within its type
                # Keep parsing until the multi-parser detects a line that doesn't match one of its sub-parsers
                if parsed_msg:
                    self.console_msg_handler(parsed_msg)
            self.log.info("EOF received from telnet.")
            pub.sendMessage("telnet_disconnected")
            self.tn.close()

    def console_msg_handler(self, parsed_msg):
        """
        Send a pubsub message for interesting messages, i.e., those with a msg_type.
        :param parsed_msg: ParsedMsg object.
        """
        if parsed_msg.msg_type:
            pub.sendMessage(parsed_msg.msg_type, msg_data=parsed_msg.msg_data)
