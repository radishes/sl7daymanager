import datetime

import db
from BasePlugin import BasePlugin
from pubsub import pub
from sqlalchemy import Column, DateTime, Integer, String, Text


class BanManager(BasePlugin):
    """
    Handles banning users and tracks bans in the datastore.
    """
    default_config = {

    }

    def __init__(self, cfg):
        super().__init__(cfg)
        pub.subscribe(self.ban, "ban_player")

    def ban(self, steamid, duration="44 years", msg="You have been banned."):
        """
        Ban a user from the server. If safe mode is enabled, nothing is done.
        :param steamid: steamid of user to ban.
        :param duration: Duration of ban.
        :param msg: Message to present to banned user.
        """
        self.log.info("Banning player {} for duration '{}'. Reason: {}".format(steamid, duration, msg))
        pub.sendMessage("telnet_cmd", cmd='ban add {} {} "{}"'.format(steamid, duration, msg))
        if not self.get_global_config("safe_mode", True):
            banned_player = BannedPlayer(steamid=steamid,
                                         duration=duration,
                                         message=msg)
            db.session.add(banned_player)
            db.commit_session()


class BannedPlayer(db.Base):
    """
    A user who has been banned from the server.
    """
    __tablename__ = "banned_player"
    id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime, default=datetime.datetime.now)
    steamid = Column(String(24))
    duration = Column(Text)
    message = Column(Text)

    def __repr__(self):
        return "steamid {steamid} was banned for {duration} on {timestamp}. " \
               "Their ban message: '{message}'".format(**self.__dict__)
