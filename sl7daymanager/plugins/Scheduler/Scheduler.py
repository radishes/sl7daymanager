import datetime
import logging
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger
from apscheduler.triggers.date import DateTrigger
from apscheduler.triggers.interval import IntervalTrigger
from pubsub import pub
from sqlalchemy import Column, ForeignKey, Integer, String, Text
import db
from BasePlugin import BasePlugin


class Scheduler(BasePlugin):
    """
    Provide the ability to schedule execution of telnet commands.
    """
    def __init__(self, cfg):
        super().__init__(cfg)
        pub.subscribe(self.add_cron_command, "schedule_cron_command")
        pub.subscribe(self.add_interval_command, "schedule_interval_command")
        pub.subscribe(self.add_onetime_command, "schedule_onetime_command")
        self.scheduler = None
        logging.getLogger("apscheduler").setLevel(logging.WARNING)
        self.scheduler = BackgroundScheduler()
        self.scheduler.start()

    def post_init(self):
        for run_at_cmd, times in self.get_config("run_at").items():
            try:
                cmd = self.get_config("cmds")[run_at_cmd]
            except KeyError:
                self.log.warning("Scheduled command '{}' is not defined and will not be run.".format(run_at_cmd))
            for run_at_time in times:
                self.add_cron_command(run_at_time, cmd)
        for run_every_cmd, times in self.get_config("run_every").items():
            try:
                cmd = self.get_config["cmds"][run_every_cmd]
            except KeyError:
                self.log.warning("Scheduled command '{}' is not defined and will not be run.".format(run_every_cmd))

            self.add_interval_command(times, cmd)

    def add_cron_command(self, hour_minute, command, replace=False):
        """
        Schedule a command to run at a given time of day.
        :param hour_minute: String representing the time of day to run this command. Example: '17:30'
        :param command: String representing the telnet command to execute. Example: 'lp'
        :param replace: If True, delete any existing jobs whose command exactly matches this one's command.
        """
        if replace:
            self.remove_job(command)
        cron_hour, cron_minute = hour_minute.split(":")
        self.scheduler.add_job(self.run_scheduled_command, CronTrigger(hour=cron_hour, minute=cron_minute), [command])
        self.log.debug("Scheduled job '{}' to run at time {}.".format(command, hour_minute))

    def add_interval_command(self, interval_seconds, command, replace=False):
        """
        Run a command at a recurring interval.
        :param interval_seconds: Run the command this often.
        :param command: String representing the telnet command to execute. Example: 'lp'
        :param replace: If True, delete any existing jobs whose command exactly matches this one's command.
        """
        if replace:
            self.remove_job(command)
        self.scheduler.add_job(self.run_scheduled_command, IntervalTrigger(seconds=interval_seconds), [command])
        self.log.debug("Scheduled job '{}' to run every {} seconds.".format(command, interval_seconds))

    def add_onetime_command(self, interval_seconds, command, replace=False):
        """
        Run a command once, after a given amount of time has elapsed.
        :param interval_seconds: Run the command after this many seconds has passed.
        :param command: String representing the telnet command to execute. Example: 'lp'
        :param replace: If True, delete any existing jobs whose command exactly matches this one's command.
        """
        if replace:
            self.remove_job(command)
        run_date = datetime.datetime.now() + datetime.timedelta(seconds=interval_seconds)
        self.scheduler.add_job(self.run_scheduled_command, DateTrigger(run_date=run_date), [command])
        self.log.debug("Scheduled one-time job '{}' to run in {} seconds.".format(command, interval_seconds))

    def run_scheduled_command(self, command):
        """
        Execute a scheduled command.
        :param command: The command to execute.
        """
        self.log.debug("Running scheduled command: {}".format(command))
        if command.startswith("say "):
            pub.sendMessage("say_server", msg=command[4:])
        else:
            pub.sendMessage("telnet_cmd", cmd=command)

    def remove_job(self, command):
        """
        Remove any scheduled jobs that exactly match the specified command.
        :param command: Job 'command to run' to match for removal.
        :return: None
        """
        active_jobs = self.scheduler.get_jobs()
        for job in active_jobs:
            if job.args[0] == command:
                job.remove()


class SchedulerCommand(db.Base):
    """
    A command which can be scheduled with the scheduler.
    """
    __tablename__ = 'scheduler_command'
    id = Column(Integer, primary_key=True)
    command = Column(Text)  # The server console command to be executed.

    def __repr__(self):
        return "<SchedulerCommand(id='{}', command='{}')>".format(self.id, self.command)


class ScheduledCommandTime(db.Base):
    """
    A command that is scheduled to run at a specified time.
    """
    __tablename__ = 'scheduled_command_time'
    id = Column(Integer, primary_key=True)
    schedule_time = Column(String(5))  # 24-hour format. Example: '18:30'.
    command_id = Column(ForeignKey("scheduler_command.id"))  # The command to run.

    def __repr__(self):
        return "<ScheduledCommandTime(schedule_time='{}', command_id='{}')>".format(self.schedule_time, self.command_id)


class ScheduledCommandInterval(db.Base):
    """
    A command that is scheduled to run at a recurring interval.
    """
    __tablename__ = 'scheduled_command_interval'
    id = Column(Integer, primary_key=True)
    interval_seconds = Column(Integer)  # Run the command every interval_seconds seconds.
    command_id = Column(ForeignKey("scheduler_command.id"))  # The command to run.

    def __repr__(self):
        return "<ScheduledCommandInterval(interval_seconds='{}', command_id='{}')>".format(self.interval_seconds,
                                                                                           self.command_id)
