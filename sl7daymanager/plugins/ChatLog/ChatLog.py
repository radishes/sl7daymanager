from pubsub import pub
from sqlalchemy import Boolean, Column, DateTime, Integer, String, Text
import db
from BasePlugin import BasePlugin


class ChatLog(BasePlugin):
    """
    Log in-game chat messages to the datastore.
    """
    def __init__(self, cfg):
        super().__init__(cfg)
        pub.subscribe(self.player_count, "player_count")
        pub.subscribe(self.chat, "chat")
        pub.subscribe(self.player_connect, "player_connect")
        pub.subscribe(self.player_disconnect, "player_disconnect")
        pub.subscribe(self.player_killed_pvp, "player_killed_pvp")
        pub.subscribe(self.player_died, "player_died")

        self.last_player_count = 0

    def player_count(self, msg_data):
        """
        Track the server's last known player count.
        """
        self.last_player_count = int(msg_data["count"])

    def chat(self, msg_data):
        """
        Process chat messages.
        """
        chat_msg = ChatMessage(timestamp=msg_data["datetime"],
                               player_name=msg_data["name"],
                               message=msg_data["chat_msg"])
        db.session.add(chat_msg)
        db.commit_session()
        if self.get_config("chat_messages_to_log_file"):
            self.log.info(chat_msg)

    def player_connect(self, msg_data):
        """
        Process player connection messages.
        """
        connect_msg = PlayerConnectionMessage(timestamp=msg_data["datetime"],
                                              steamid=msg_data["steamid"],
                                              player_name=msg_data["name"],
                                              ip=msg_data["ip"],
                                              connect=True,
                                              disconnect=False)
        db.session.add(connect_msg)
        db.commit_session()
        self.last_player_count += 1
        if self.get_config("chat_messages_to_log_file"):
            self.log.info("{connect_msg}. {player_count} players are online.".format(connect_msg=connect_msg,
                                                                                     player_count=self.last_player_count))

    def player_disconnect(self, msg_data):
        """
        Process player disconnection messages.
        """
        disconnect_msg = PlayerConnectionMessage(timestamp=msg_data["datetime"],
                                                 steamid=msg_data["steamid"],
                                                 player_name=msg_data["name"],
                                                 connect=False,
                                                 disconnect=True)
        db.session.add(disconnect_msg)
        db.commit_session()
        self.last_player_count -= 1
        if self.get_config("chat_messages_to_log_file"):
            self.log.info("{disconnect_msg}. {player_count} players are online.".format(disconnect_msg=disconnect_msg,
                                                                                        player_count=self.last_player_count))

    def player_killed_pvp(self, msg_data):
        """
        Process messages resulting from a player killing another player.
        """
        player_death = PlayerDeath(timestamp=msg_data["datetime"],
                                   victim=msg_data["victim"],
                                   killer=msg_data["killer"],
                                   pvp=True)
        db.session.add(player_death)
        db.commit_session()
        if self.get_config("chat_messages_to_log_file"):
            self.log.info(player_death)

    def player_died(self, msg_data):
        """
        Process messages resulting from a player dying a non-PVP death.
        """
        player_death = PlayerDeath(timestamp=msg_data["datetime"],
                                   victim=msg_data["victim"],
                                   pvp=False)
        db.session.add(player_death)
        db.commit_session()
        if self.get_config("chat_messages_to_log_file"):
            self.log.info(player_death)


class ChatMessage(db.Base):
    """
    A message uttered in the game chat.
    """
    __tablename__ = 'chat_message'
    id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime)
    player_name = Column(Text)
    message = Column(Text)

    def __repr__(self):
        return "  > {}: {}".format(self.player_name, self.message)


class PlayerConnectionMessage(db.Base):
    """
    A player connect or disconnect message.
    """
    __tablename__ = 'connect_message'
    id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime)
    steamid = Column(String(24))
    player_name = Column(Text)
    ip = Column(String(15))
    connect = Column(Boolean)
    disconnect = Column(Boolean)

    def __repr__(self):
        if self.connect:
            msg = ">>> Player '{player_name}' has joined the server. ({steamid} : {ip})"
        else:
            msg = ">>> Player '{player_name}' has left the server. ({steamid})"
        return msg.format(**self.__dict__)


class PlayerDeath(db.Base):
    """
    A player died.
    """
    __tablename__ = 'player_death'
    id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime)
    victim = Column(Text)
    killer = Column(Text)
    pvp = Column(Boolean, default=False)

    def __repr__(self):
        if self.pvp:
            msg = " >> Player '{victim}' was killed by player '{killer}'."
        else:
            msg = " >> Player '{victim}' died."
        return msg.format(**self.__dict__)
