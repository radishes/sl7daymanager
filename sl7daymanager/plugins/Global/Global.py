from BasePlugin import BasePlugin


class Global(BasePlugin):
    """
    Do-nothing "plugin". Only exists to provide global config values via its default_config.yaml.
    """
    def __init__(self, cfg):
        super().__init__(cfg)
