from pubsub import pub
from sqlalchemy import Column, Integer, String
import db
from BasePlugin import BasePlugin


class PingMonitor(BasePlugin):
    """
    Maintains a moving list of the last player_ping_n ping values for each player. Players whose ping exceeds a
    specified threshold for too long are banned from the server. Individual players may be whitelisted so that they
    are never banned for high ping.
    """
    def __init__(self, cfg):
        self.requires = ["PlayerData"]
        super().__init__(cfg)
        pub.subscribe(self.player_state, "player_state")
        self.ping_whitelist = set()  # Set of ping-whitelisted steamids.
        self.player_ping_tail = {}  # Key: steamid; Value: list of last n pings
        self.player_ping_n = self.get_config("player_ping_n")
        self.ping_action_threshold = self.get_config("ping_action_threshold")

    def post_init(self):
        for whitelisted_steamid in self.get_config("whitelisted_steamids", []):
            self.ping_whitelist.add(str(whitelisted_steamid))
        self.log.info("Loaded {} whitelisted steamids from configs.".format(len(self.ping_whitelist)))

    def player_state(self, msg_data):
        """
        Record the player's ping. If they have accumulated enough ping values, and if all the values are above the ping
        threshold, then ban the player unless they are on the ping whitelist.
        """
        ping = int(msg_data["ping"])
        steamid = msg_data["steamid"]
        player_name = msg_data["name"]
        if steamid in self.ping_whitelist:
            return
        if steamid not in self.player_ping_tail:
            self.player_ping_tail[steamid] = [ping]
            self.log.debug("Added player '{}' to ping tail list.".format(player_name))
        else:
            self.player_ping_tail[steamid].append(ping)
            self.player_ping_tail[steamid] = self.player_ping_tail[steamid][-self.player_ping_n:]
            num_pings = len(self.player_ping_tail[steamid])
            if num_pings >= self.player_ping_n:
                if all(p > self.ping_action_threshold for p in self.player_ping_tail[steamid]):
                    # Update local ping whitelist cache from database.
                    ping_whitelist_query = db.session.query(PingWhitelist.steamid).all()
                    self.ping_whitelist.update([p[0] for p in ping_whitelist_query])
                    if steamid in self.ping_whitelist:
                        return
                    self.log.info("Banning player '{}' for high ping.".format(player_name))
                    pub.sendMessage("ban_player", steamid=steamid, msg=self.get_config("ping_action_message"))
                    pub.sendMessage("say_server", msg=self.get_config("ping_action_announcement")
                                    .format(player_name=player_name))

    def player_disconnect(self, msg_data):
        """
        Clean up a disconnected player's ping record.
        """
        steamid = msg_data["steamid"]
        player_name = msg_data["name"]
        try:
            del self.player_ping_tail[steamid]
            self.log.debug("Deleted player '{}' from ping tail list.".format(player_name))
        except KeyError:
            self.log.debug("Unable to delete player '{}' from ping tail list.".format(player_name))

    def whitelist_player(self, steamid):
        """
        Add a player to the ping whitelist.
        """
        ping_whitelist = PingWhitelist(steamid=steamid)
        db.session.add(ping_whitelist)
        db.commit_session()
        self.log.info("Added player to ping whitelist: {}".format(steamid))


class PingWhitelist(db.Base):
    """
    A ping-whitelisted player.
    """
    __tablename__ = 'ping_whitelist'
    id = Column(Integer, primary_key=True)
    steamid = Column(String(24), unique=True)  # Steam64 ID.

    def __repr__(self):
        return "<PingWhitelist(steamid='{}')>".format(self.steamid)
