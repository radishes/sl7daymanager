import geoip2.database
from pubsub import pub
from sqlalchemy import Column, Integer, String, Text
import db
from BasePlugin import BasePlugin


class Geolookup(BasePlugin):
    """
    Provide geolocation service for player IP addresses. Automatically ban players from specified countries.
    """
    def __init__(self, cfg):
        super().__init__(cfg)
        pub.subscribe(self.player_disconnect, "player_disconnect")
        pub.subscribe(self.player_state, "player_state")
        self.validated_steamids = []  # Steamids of currently active players on the server who have passed geo ip filtering.
        self.cant_validate_steamids = []  # Steamids of currently active players on the server whose address can't be validated.
        self.reader = geoip2.database.Reader(self.cfg["Geolookup"]["geolite_database_file"])

    def ip_geolookup(self, ip):
        """
        Geolocate an IP address using the Geolite database file.
        :param ip: String representing an IP address.
        :return: A dict containing the IP's country_code and country_name.
        """
        response = self.reader.country(ip)
        return {'country_code': response.country.iso_code, 'country_name': response.country.name}

    def player_state(self, msg_data):
        """
        Attempt to geolocate this player's IP. Players whose IP resolves to a banned country will be banned from the
        server.
        """
        steamid = msg_data["steamid"]
        ip = msg_data["ip"]
        player_name = msg_data["name"]
        if steamid not in self.validated_steamids and steamid not in self.cant_validate_steamids:
            try:
                ip_data_api = self.ip_geolookup(ip)
                ip_geo = IPGeo(steamid=steamid, ip=ip, **ip_data_api)
                db.session.add(ip_geo)
                db.commit_session()
                lookup_log_msg = "Completed geolookup on player '{}' from {}. ({} : {})".format(player_name,
                                                                                                ip_data_api["country_name"],
                                                                                                steamid,
                                                                                                ip)
                if self.get_config("log_geolookup_messages_to_info"):
                    self.log.info(lookup_log_msg)
                else:
                    self.log.debug(lookup_log_msg)
                if (ip_data_api["country_code"] in self.get_config("banned_country_codes")) or \
                   (ip_data_api["country_name"] in self.get_config("banned_country_names")):
                    self.log.info("Banning player '{}' from {}".format(player_name, ip_data_api["country_name"]))
                    pub.sendMessage("ban_player",
                                    steamid=steamid,
                                    msg=self.get_config("ban_message").format(player_name=player_name,
                                                                              country_name=ip_data_api["country_name"]))
                    pub.sendMessage("say_server",
                                    msg=self.cfg["Geolookup"]["ban_announcement"].format(player_name=player_name,
                                                                                         country_name=ip_data_api["country_name"]),
                                    color=None)
                else:
                    self.validated_steamids.append(steamid)
            except geoip2.errors.AddressNotFoundError as e:
                self.cant_validate_steamids.append(steamid)
                self.log.info("Unable to perform IP Geolookup on player '{}'. Reason: {}".format(player_name, e))

    def player_disconnect(self, msg_data):
        """
        When a player disconnects from the server, remove them from the list of geo-authenticated players.
        """
        self.log.debug("Processing player_disconnect message: {}".format(msg_data))
        steamid = msg_data["steamid"]
        try:
            self.validated_steamids.remove(steamid)
        except ValueError:
            pass  # Value not in list.
        try:
            self.cant_validate_steamids.remove(steamid)
        except ValueError:
            pass  # Value not in list.


class IPGeo(db.Base):
    """
    Data obtained from the IP lookup of a player's IP.
    """
    __tablename__ = 'ip_geo'
    id = Column(Integer, primary_key=True)
    ip = Column(String(64), unique=True)
    steamid = Column(String(24))
    city = Column(Text)
    country_code = Column(Text)
    country_name = Column(Text)
    latitude = Column(Text)
    longitude = Column(Text)
    metro_code = Column(Text)
    region_code = Column(Text)
    region_name = Column(Text)
    time_zone = Column(Text)
    zip_code = Column(Text)

    def __repr__(self):
        return "<IPGeo(ip='%s', country_code='%s', country_name='%s')>" % (self.ip,
                                                                           self.country_code,
                                                                           self.country_name)
