import datetime
from copy import deepcopy
from pubsub import pub
from sqlalchemy import Boolean, Column, DateTime, Float, Integer, String, Text
import db
from BasePlugin import BasePlugin


class PlayerData(BasePlugin):
    """
    Track the output of the 'lp' (list players) command for each player, and store it to the datastore.
    """
    def __init__(self, cfg):
        super().__init__(cfg)
        pub.subscribe(self.player_state, "player_state")
        pub.subscribe(self.telnet_connected, "telnet_connected")

    def telnet_connected(self):
        """
        Schedule the recurring execution of the 'lp' command.
        """
        pub.sendMessage("telnet_cmd", cmd="lp")  # Run 'lp' immediately, so that we have a starting dataset.
        # Schedule 'lp' to run on a regular basis.
        pub.sendMessage("schedule_interval_command",
                        interval_seconds=self.get_config("player_polling_interval_seconds"),
                        command="lp")

    def player_state(self, msg_data):
        """
        Add the player's state information from the 'lp' command to the datastore.
        """
        self.log.debug("Processing player_state message: {}".format(msg_data))
        player_query = db.session.query(Player).filter(Player.steamid == msg_data['steamid'])
        if player_query.count() == 0:
            # This player hasn't been seen before.
            player_data = {"steamid": msg_data["steamid"],
                           "id_server": msg_data["id"]}
            player = Player(**player_data)
            db.session.add(player)
            db.commit_session()

        player_state_data = deepcopy(msg_data)
        del player_state_data["lp_index"], player_state_data["id"]
        player_state_data["remote"] = bool(player_state_data["remote"])
        player_state_obj = PlayerState(**player_state_data)
        db.session.add(player_state_obj)
        db.commit_session()


class PlayerState(db.Base):
    """
    A status snapshot of one player. This is parsed from the result of the 'lp' command.
    """
    __tablename__ = 'player_state'
    id = Column(Integer, primary_key=True)
    steamid = Column(String(24))
    create_date = Column(DateTime, default=datetime.datetime.now)
    name = Column(Text)
    pos_x = Column(Float)
    pos_y = Column(Float)
    pos_z = Column(Float)
    rot_x = Column(Float)
    rot_y = Column(Float)
    rot_z = Column(Float)
    remote = Column(Boolean)
    health = Column(Integer)
    deaths = Column(Integer)
    zombies = Column(Integer)
    players = Column(Integer)
    score = Column(Integer)
    level = Column(Integer)
    ip = Column(Text)
    ping = Column(Integer, nullable=True)

    regular_columns = ["remote", "health", "deaths", "zombies", "players", "score", "level", "ip", "ping"]

    def __repr__(self):
        return "<PlayerState(name='{}', ip='{}')>".format(self.name, self.ip)


class Player(db.Base):
    """
    Fundamental information on a player.
    """
    __tablename__ = 'player'
    id = Column(Integer, primary_key=True)
    create_date = Column(DateTime, default=datetime.datetime.now)
    id_server = Column(Integer)  # Local ID on the server.
    steamid = Column(String(24))  # Steam64 ID.

    def __repr__(self):
        return "<Playersteamid='{}')>".format(self.steamid)
