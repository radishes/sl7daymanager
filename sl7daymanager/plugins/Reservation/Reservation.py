from pubsub import pub
from BasePlugin import BasePlugin


class Reservation(BasePlugin):
    """
    Temporarily expand the server's player capacity on demand. Revert it back to the normal value after a period.
    """
    def __init__(self, cfg):
        self.requires = ["PlayerData"]
        super().__init__(cfg)
        pub.subscribe(self.telnet_connected, "telnet_connected")
        pub.subscribe(self.add_player_capacity, "add_player_capacity")
        pub.subscribe(self.player_count, "player_count")
        pub.subscribe(self.game_pref, "game_pref")
        pub.subscribe(self.player_limit_exceeded, "player_limit_exceeded")

        self.last_player_count = 0
        self.last_player_capacity = 0

    def telnet_connected(self):
        pub.sendMessage("schedule_interval_command",
                        interval_seconds=self.get_config("game_pref_polling_interval_seconds"),
                        command="gg",
                        replace=True)

    def game_pref(self, msg_data):
        """
        Parse the server's last known player capacity from the 'gg' command.
        """
        if msg_data["key"] == "ServerMaxPlayerCount":
            self.last_player_capacity = int(msg_data["value"])

    def player_count(self, msg_data):
        """
        Track the server's last known player count.
        """
        self.last_player_count = int(msg_data["count"])

    def player_limit_exceeded(self, msg_data):
        """
        A player tried to join the server, but the server was full.
        """
        if msg_data["steamid"] in str(self.get_config("reservation_enabled_steamids")):
            self.add_player_capacity()

    def add_player_capacity(self):
        """
        Increase the server's player capacity by one. Schedule a job to later revert it back to its normal value. The
        capacity increase request will be refused if the server's capacity is already at its hard maximum.
        """
        self.log.info("Adding player capacity!")
        last_player_count = self.last_player_count if not self.last_player_capacity else self.last_player_capacity
        if last_player_count < self.get_config("hard_capacity_limit"):
            new_capacity = 1 + (last_player_count if last_player_count > self.get_config("player_capacity_base")
                                                  else self.get_config("player_capacity_base"))
            self.log.info("Setting player capacity to {} per request...".format(new_capacity))
            pub.sendMessage("telnet_cmd", cmd="sg ServerMaxPlayerCount {}".format(new_capacity))
            self.last_player_capacity = new_capacity
            pub.sendMessage("schedule_onetime_command",
                            interval_seconds=int(self.get_config("revert_capacity_seconds")),
                            command="sg ServerMaxPlayerCount {}".format(self.get_config("player_capacity_base")),
                            replace=True)
        else:
            self.log.info("Capacity increase request refused. Maximum capacity reached Current players: %s." % last_player_count)
