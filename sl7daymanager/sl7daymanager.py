"""
Entry point for sl7daymanager application.

 > python sl7daymanager.py
"""
from __future__ import print_function
import argparse
import logging
import os
import sys
from logging.handlers import RotatingFileHandler
from pprint import pprint
from ruamel import yaml
from manager import Manager, ConfigNotFoundError


def init_logging(log_path, log_level):
    log_format = "%(asctime)s [%(module)s::%(levelname)s]  %(message)s"
    log = logging.getLogger("")
    log.setLevel(log_level)
    fmt = logging.Formatter(log_format)

    ch = logging.StreamHandler(sys.stdout)
    ch.setFormatter(fmt)
    log.addHandler(ch)

    fh = RotatingFileHandler(log_path, maxBytes=(1024*1024*10))
    fh.setFormatter(fmt)
    log.addHandler(fh)


def parse_arguments():
    parser = argparse.ArgumentParser(description="Management and monitoring tool for 7 Days to Die game server.")
    parser.add_argument("--run", "-r", action="store_true",
                        help="Run the management system. Configuration information will be loaded from files in the "
                             "config/ directory. The files are loaded in ascending alphabetical order; when duplicate "
                             "config values exist, the last loaded value takes precedence.")
    parser.add_argument("-d", "--dump_config", action="store_true",
                        help="Print the config values that will be used when run.")
    parser.add_argument("-g", "--generate_default_config", nargs="?", const="config.yaml", type=str,
                        help="Generate a config file with default values. The file will be named 'config.yaml' if no "
                             "other name is provided. This file will be generated into the config/ directory. If the "
                             "file already exists, nothing will be done. Other arguments are ignored if this flag is "
                             "set.")
    parser.add_argument("-o", "--log_file", default="./sl7daymanager.log",
                        help="Log file path.")
    parser.add_argument("-l", "--log_level", default="INFO", choices=["DEBUG", "INFO", "WARNING", "ERROR"],
                        help="Log verbosity level. INFO and DEBUG are the most useful.")
    return vars(parser.parse_args())


def create_default_config_file(default_config_path):
    if os.path.exists(default_config_path):
        print("Error: File '{}' already exists. I won't overwrite it. To fix this, delete the existing file "
              "and re-run, or provide a different config file name with the '-g' flag.".format(default_config_path))
        sys.exit(1)
    default_config = Manager.generate_default_config()

    with open(default_config_path, "w") as f:
        yaml.dump(default_config, f, default_flow_style=False, indent=4, Dumper=yaml.RoundTripDumper)


def start():
    """
    Parse command line arguments and execute the program based on the provided arguments. 
    """
    os.chdir(os.path.dirname(__file__))
    args = parse_arguments()
    init_logging(args["log_file"], args["log_level"])
    log = logging.getLogger(__name__)
    log.info("=" * 80)
    log.info("Starting up sl7daymanager!")

    if not os.path.isdir("config"):
        os.mkdir("config")

    if args["generate_default_config"]:
        create_default_config_file(os.path.join("config", args["generate_default_config"]))
        sys.exit(0)

    try:
        manager = Manager("config")
    except ConfigNotFoundError:
        default_config_path = os.path.join("config", "config.yaml")
        print("No valid configuration files were found in the config directory. This is normal if this is your first "
              "time running the application. I'm going to generate a default config file for you.")
        create_default_config_file(default_config_path)
        print("New config file generated as '{}'. This file must be edited before it can be used. In particular, "
              "update the Telnet section of the config file.".format(default_config_path))
        sys.exit(0)

    if args["dump_config"]:
        pprint(manager.cfg)

    if args["run"]:
        manager.start()


if __name__ == "__main__":
    start()
