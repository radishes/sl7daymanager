"""
Database globals and helper functions.
"""
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.exc import IntegrityError


# SQLAlchemy objects
engine = None
Base = declarative_base()
Session = sessionmaker()
session = None


def init_session(connection_string):
    global engine
    global session
    engine = create_engine(connection_string, encoding='utf-8')

    Base.metadata.create_all(engine)
    Session.configure(bind=engine)
    session = Session()


def commit_session():
    try:
        session.commit()
    except IntegrityError:
        session.rollback()  # Eat duplicate key errors.
