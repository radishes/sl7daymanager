import logging
import os
import config
import db
from load_module import get_class_by_name


log = logging.getLogger(__name__)


class ConfigNotFoundError(RuntimeError):
    pass


class Manager:
    """
    Plugin manager: loads all active plugins, initializes them, and hands control over to the Telnet plugin.
    """
    def __init__(self, config_path):
        self.required_plugins = ["Telnet", "Scheduler"]  # These plugins are always loaded.

        self.cfg = config.load_config(config_path)  # Dict of config settings.
        self.plugin_modules = {}  # Key: plugin module name. Value: module object.
        self.plugins = {}  # Key: plugin name. Value: plugin class instance.

        if not self.cfg:
            raise ConfigNotFoundError
        else:
            self.register_plugins()

    def register_plugins(self):
        """
        Load the plugin modules defined in the config's global.enabled_plugins setting.
        """
        self.cfg["Global"]["enabled_plugins"] = self.required_plugins + self.cfg["Global"]["enabled_plugins"]
        for plugin_name in self.cfg["Global"]["enabled_plugins"]:
            plugin_module_name = "plugins.{plugin_name}.{plugin_name}".format(plugin_name=plugin_name)
            self.plugin_modules[plugin_name] = get_class_by_name(plugin_module_name, plugin_name)

    def launch_plugins(self):
        """
        Instantiate all active plugins.
        """
        for plugin_name, plugin_module in self.plugin_modules.items():
            try:
                log.info("Initializing plugin '{}'.".format(plugin_name))
                self.plugins[plugin_name] = plugin_module(self.cfg)
            except TypeError:
                log.error("Unable to load plugin '{}'. Is this a valid plugin?".format(plugin_name))

    def start(self):
        """
        Perform final initialization and hand over control to the Telnet plugin. 
        :return: 
        """
        db.init_session(self.cfg["Global"]["database_connection_string"])
        self.launch_plugins()

        for plugin in self.plugins.values():
            if hasattr(plugin, "post_init"):
                plugin.post_init()

        self.plugins["Telnet"].start()

    @staticmethod
    def generate_default_config():
        """
        This attempts to generate a default config for every plugin in the plugins directory, regardless of whether the 
        plugin enabled.
        :return: Merged config dict. 
        """
        plugins_dir = "plugins"
        default_config = {}
        plugin_classes = {}  # Key: plugin name. Value: plugin class.
        for path in os.listdir(plugins_dir):
            # Populate plugin_classes with all the plugins in the plugins directory.
            if os.path.isdir(os.path.join(plugins_dir, path)):
                plugin_module_name = "{plugins_dir}.{plugin_name}.{plugin_name}".format(plugins_dir=plugins_dir,
                                                                                        plugin_name=path)
                plugin_class = get_class_by_name(plugin_module_name, path)
                if plugin_class:
                    plugin_classes[path] = plugin_class

        for plugin_name, plugin_module in plugin_classes.items():
            # Ask each plugin to dump its default config.
            if hasattr(plugin_module, "load_config"):
                plugin_default_config = plugin_module.load_config()
                if plugin_default_config:
                    default_config[plugin_name] = plugin_default_config
        return default_config
