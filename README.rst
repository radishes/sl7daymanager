=============
sl7daymanager
=============
A server management and monitoring tool for 7 Days to Die.
----------------------------------------------------------
sl7daymanager is a Python application which connects to your 7 Days to Die game server via telnet. By listening to the
telnet feed, and communicating with the game server via telnet, sl7daymanager is able to provide a number of tools to
level up your ability to manage your 7dtd server.


Requirements
------------
- Python3, along with a few dependencies which pip can install for you.
- Your 7dtd game server application must run with telnet enabled.


Features
--------
- Connects to any 7dtd game server for which you have the telnet password. You do **not** need access to the 7dtd game server host, so sl7daymanager should work with budget game server providers. You can run sl7daymanager from your home PC if you like.
- Free and open source!
- Cross platform. Should work with any system which can run Python. Tested on Linux and Windows.
- Collects a bunch of data from your game server and puts it in a database (sqlite local file by default). You can review this data to glean valuable insights from your server and players. Want to know where any player was at any given time? Check the database!
- Comes bundled with a bunch of nifty plugins. The modular plugin design lets you enable and disable plugins as you see fit. See the plugin list below for more details on specific features.
- Extensible: write your own plugins if you want.


===========
Plugin List
===========
All these plugins come with sl7daymanager. All are enabled by default.

Telnet
------
The Telnet plugin is the core plugin which enables everything else to work. It listens to the game server's telnet feed
for interesting messages. When an interesting message comes along, this plugin parses it and broadcasts it out for
other plugins to use as they see fit. Other plugins can send messages to the game server through the Telnet plugin, as
well.

Scheduler
---------
Scheduler lets you schedule commands to be executed at a specific time of day or at a recurring interval. A common use
is to broadcast canned chat messages to all players from the server. Other plugins also schedule tasks through the
Scheduler.

PlayerData
----------
PlayerData is a powerful plugin that simply schedules the command 'lp' (list players) to be executed on a configurable
interval (every 10 seconds by default). This command provides a bunch of information about every player on the server,
including their exact location in the game world, their health, their player and zombie kills, their deaths, and so on.
All of this data is stored in the database, and can be manually reviewed to effectively track a player's status at any
given time. When combined with a data visualization tool, this data is extremely powerful, allowing for example, all
players' movements in the game world to be visualized.

Reservation
-----------
Reservation provides a sort of "reserved slot" service. This works by temporarily increasing the server's player
capacity. After a set period of time, the capacity will be reverted to its normal baseline value.

This plugin can be utilized in a couple of different ways:

1. Reservation itself lets you specify a list of player steamids; when a player from this list tries to join the game
server, but is rejected because the server is at its player capacity, Reservation will see this happen and immediately
temporarily increase the server's player capacity by 1. The player can then immediately attempt to join the server
again.

2. Other plugins can use Reservation to increase capacity based on their own criteria. For example, the Web plugin can
provide a webpage with a button that can tempoorarily increase capacity when pressed.

Geolookup
---------
Geolookup lets you auto-ban players from countries you specify. Experienced 7dtd server admins know that cheaters in
this game are notoriously concentrated in certain parts of the world; this plugin lets you do something about it. This
is accomplished by performing a geo-lookup of all players' IP addresses. Of course, clever players can easily mask
their location by using a VPN or similar service, so this isn't a silver bullet. However, combined with the PingMonitor
and NetworkBlacklist plugins, it's possible to significantly reduce cheating.

PingMonitor
-----------
PingMonitor lets you specify a threshold ping. Players whose ping is above the threshold for a certain period of time
are auto-banned. You can whitelist specific players by steamid, and they will not be banned for high ping. Cheaters
often mask their IPs using a VPN, and so evade the Geolookup plugin, but they can't mask their ping.

NetworkBlacklist
----------------
NetworkBlacklist allows you to ban IP addresses and IP ranges from your server. Blacklisted networks are specified in
the form of a single IP address ('1.2.3.4'), or a network using slash notation ('1.2.3.4/24'). Players who join from a
blacklisted network are auto-banned. If you've identified a VPN service regularly used by cheaters, for example, you
can blacklist their entire network range.

BanManager
----------
BanManager provides auto-banning capabilities to other plugins. Bans are tracked in the database.

ChatLog
-------
ChatLog logs all chat messages and player join/quit messages. These are output to the log file and stored in the
database.

CommandLogger
-------------
CommandLogger logs all administrative commands issued by any player or system (including sl7daymanager.) This includes
commands issued in the game console and commands sent via telnet. All issued commands are tracked in the database.
